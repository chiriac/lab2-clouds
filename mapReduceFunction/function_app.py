from collections import defaultdict
import azure.functions as func
import azure.durable_functions as df
from typing import List, Tuple
from itertools import chain
from azure.storage.blob import BlobServiceClient

myApp = df.DFApp(http_auth_level=func.AuthLevel.ANONYMOUS)

# An HTTP-Triggered Function with a Durable Functions Client binding
@myApp.route(route="orchestrators/{functionName}")
@myApp.durable_client_input(client_name="client")
async def http_start(req: func.HttpRequest, client):
    function_name = req.route_params.get('functionName')
    instance_id = await client.start_new(function_name)
    response = client.create_check_status_response(req, instance_id)
    return response

# Orchestrator
@myApp.orchestration_trigger(context_name="context")
def MasterOrchestrator(context):
    connection_string = "DefaultEndpointsProtocol=https;AccountName=mapreducestore;AccountKey=I+2k43fuxys6NHuDSRhIBqr6PL8nvb7WE83YhSqczhmJ03VwnnNLejhAK6XaLe3lAOAwzA05UFhS+AStLmmlPw==;EndpointSuffix=core.windows.net"

    lines = yield context.call_activity("GetInputDataFn", connection_string)

    mapper_tasks = []
    for line in lines:
        mapper_tasks.append(context.call_activity("MapperActivity", [line]))
    words = yield context.task_all(mapper_tasks)

    # Had to flatten the list because I couldn't pass Tuples as arguments nor
    # use extend instead of append to have a flat list from the start
    counts = yield context.call_activity("ShufflerActivity", list(chain.from_iterable(words)))
    
    reducer_tasks = []
    for count in counts:
        reducer_tasks.append(context.call_activity("ReducerActivity", [count]))
    reduced = yield context.task_all(reducer_tasks)

    # Had to flatten the list because I couldn't pass Tuples as arguments nor
    # use extend instead of append to have a flat list from the start
    return list(chain.from_iterable(reduced))


@myApp.activity_trigger(input_name="lines")
def MapperActivity(lines: List[Tuple[int, str]]) -> List[Tuple[str, int]]:
    word_count = []
     
    # Passed a list with only one element as an input because i cannot pass a Tuple
    line = lines[0]
    words = line[1].split()
    for word in words:
        word_count.append((word, 1))

    return word_count


@myApp.activity_trigger(input_name="words")
def ShufflerActivity(words: List) -> List[Tuple[str, List[int]]]:
    word_count = defaultdict(list)

    for word, count in words:
        word_count[word].append(count)

    aggregated_list = [(word, counts) for word, counts in word_count.items()]

    return aggregated_list


@myApp.activity_trigger(input_name="counts")
def ReducerActivity(counts: List[Tuple[str, List[int]]]) -> List[Tuple[str, int]]:
    reduced_list = []

    # Passed a list with only one element as an input because i cannot pass a Tuple
    count = counts[0]
    
    reduced_list.append((count[0], sum(count[1])))

    return reduced_list

@myApp.activity_trigger(input_name="connectionString")
def GetInputDataFn(connectionString: str) -> List[Tuple[int, str]]:
    lines = []
    line_count = 0
    container_name = "mapreducecontainer"

    blob_service_client = BlobServiceClient.from_connection_string(connectionString)
    container_client = blob_service_client.get_container_client(container_name)

    for blob in container_client.list_blobs():
        blob_client = container_client.get_blob_client(blob.name)
        blob_data = blob_client.download_blob().readall().decode('utf-8').split('\n')
        for line in blob_data:
                        line_count += 1
                        lines.append((line_count, line.strip()))
    
    return lines