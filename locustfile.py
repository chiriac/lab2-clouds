import time
from locust import HttpUser, task, between

class QuickstartUser(HttpUser):

	@task
	def hello_world(self):
		# response = self.client.get("/numericalintegralservice/0/3.14159")

		response = self.client.get("/api/httpintegralfunction?lower=0&upper=3.14159")
		# print(response.content)