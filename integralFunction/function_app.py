import azure.functions as func
import logging
import math
import json

app = func.FunctionApp(http_auth_level=func.AuthLevel.ANONYMOUS)

def numerical_integration(lower, upper, N):
    delta_x = (upper - lower) / N
    integral_sum = 0.0

    for i in range(N):
        x_i = lower + i * delta_x
        integral_sum += abs(math.sin(x_i)) * delta_x

    return integral_sum

@app.route(route="HttpIntegralFunction")
def HttpIntegralFunction(req: func.HttpRequest) -> func.HttpResponse:
    logging.info('Python HTTP trigger function processed a request.')

    lower = req.params.get('lower')
    upper = req.params.get('upper')
    if not lower:
        try:
            req_body = req.get_json()
        except ValueError:
            pass
        else:
            lower = req_body.get('lower')

    if not upper:
        try:
            req_body = req.get_json()
        except ValueError:
            pass
        else:
            upper = req_body.get('upper')


    if lower is not None and upper is not None: 
        lower_bound = float(lower)
        upper_bound = float(upper)
        N_values = [10, 100, 1000, 10000, 100000, 1000000]

        results = {}
        for N in N_values:
            result = numerical_integration(lower_bound, upper_bound, N)
            results[f"N={N}"] = result

        json_response = json.dumps(results)

        return func.HttpResponse(json_response, mimetype="application/json")