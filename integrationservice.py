from flask import Flask, request
import math

app = Flask(__name__)

def numerical_integration(lower, upper, N):
    delta_x = (upper - lower) / N
    integral_sum = 0.0

    for i in range(N):
        x_i = lower + i * delta_x
        integral_sum += abs(math.sin(x_i)) * delta_x

    return integral_sum

@app.route('/numericalintegralservice/<lower>/<upper>', methods=['GET'])
def perform_integration(lower, upper):
    lower_bound = float(lower)
    upper_bound = float(upper)
    N_values = [10, 100, 1000, 10000, 100000, 1000000]

    results = {}
    for N in N_values:
        result = numerical_integration(lower_bound, upper_bound, N)
        results[f"N={N}"] = result

    return results

if __name__ == '__main__':
    app.run(debug=True)
