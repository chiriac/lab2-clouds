import math

def numerical_integration(lower, upper, N):
    delta_x = (upper - lower) / N
    integral_sum = 0.0

    for i in range(N):
        x_i = lower + i * delta_x
        integral_sum += abs(math.sin(x_i)) * delta_x

    return integral_sum

# Define the interval
lower_bound = 0
upper_bound = math.pi  # approximately π

# List of values for N
N_values = [10, 100, 1000, 10000, 100000, 1000000]

# Perform numerical integration for each N value
for N in N_values:
    result = numerical_integration(lower_bound, upper_bound, N)
    print(f"For N = {N}: Result = {result}")
